package jave.fancy.jweather;

import android.app.TabActivity;
import android.content.Intent;
import android.os.Bundle;
import android.widget.RadioGroup;
import android.widget.RadioGroup.OnCheckedChangeListener;
import android.widget.TabHost;

public class MainActivity extends TabActivity {
	private RadioGroup group;
	private TabHost tabHost;
	public static final String TAB_HOME="tabHome";
	public static final String TAB_ZHISHU="tab_zhishu";
	public static final String TAB_QUSHI="tab_qushi";
	public static final String TAB_GONGJU="tab_gongJu";
    /** Called when the activity is first created. */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);
        group = (RadioGroup)findViewById(R.id.main_radio);
		tabHost = getTabHost();
		tabHost.addTab(tabHost.newTabSpec(TAB_HOME)
	                .setIndicator(TAB_HOME)
	                .setContent(new Intent(this,TianQi.class)));
	    tabHost.addTab(tabHost.newTabSpec(TAB_ZHISHU)
	                .setIndicator(TAB_ZHISHU)
	                .setContent(new Intent(this,ZhiShu.class)));
	    tabHost.addTab(tabHost.newTabSpec(TAB_QUSHI)
	    		.setIndicator(TAB_QUSHI)
	    		.setContent(new Intent(this,Qushi.class)));
	    tabHost.addTab(tabHost.newTabSpec(TAB_GONGJU)
	    		.setIndicator(TAB_GONGJU)
	    		.setContent(new Intent(this,GongJu.class)));
	    group.setOnCheckedChangeListener(new OnCheckedChangeListener() {
			public void onCheckedChanged(RadioGroup group, int checkedId) {
				switch (checkedId) {
				case R.id.radio_button0:
					tabHost.setCurrentTabByTag(TAB_HOME);
					break;
				case R.id.radio_button1:
					tabHost.setCurrentTabByTag(TAB_ZHISHU);
					break;
				case R.id.radio_button2:
					tabHost.setCurrentTabByTag(TAB_QUSHI);
					break;
				case R.id.radio_button3:
					tabHost.setCurrentTabByTag(TAB_GONGJU);
					break;
				case R.id.radio_button4:
					break;
				default:
					break;
				}
			}
		});
    }
}